import { Sequelize } from "sequelize";

const db = new Sequelize("auth_db", "root", "hikendev", {
  host: "localhost",
  dialect: "mysql",
});

export default db;
